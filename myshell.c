/*
 *myshell详细设计实现 
 */

#include "myshell.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pthread.h>
#include <setjmp.h>
#include "builtin.h"
#include "global.h"

//处理中断信号的函数，重新接收命令 
void sigint_handler(int sig) 
{
	printf("\n3150104669ckczzj@myshell:%s$ ",PATHVAR[1]);
	fflush(stdout);
}

//父进程处理暂停信号的函数 
void sigtstp_handler(int sig) 
{
	if(JOB[pidindex]==NULL) exit(0); //若当前进程已结束，直接退出 
	STAT[pidindex]=PAUSE; //状态改为暂停 
	printf("\n%d has stopped!\n",PID[pidindex]);
	char temp[1024];
	sprintf(temp,"kill -STOP %d",PID[pidindex]); //给当前进程发送暂停信号 
	system(temp);
	cmd_count=0; //子进程暂停后，父进程可能已经进入等待状态，陷入困局，所以进行跳转，cmd_count=0;跳转至execute_command()开头，直接结束该函数 
	longjmp(jumper,0);
}

//配置信号处理函数 
void setup(void)
{
	signal(SIGINT,sigint_handler); //中断信号
	signal(SIGQUIT,SIG_IGN); //忽略ctrl+/
}

//设置PATH环境变量 
void setpath(char* s)
{
	strcpy(PATHVAR[0],s);
}

//获取第i条命令，放入cmd[i]中 
void get_command(int i) 
{
	int j=0;
	int inword;
	while(*cmdp!='\0')
	{
		while(*cmdp==' '||*cmdp=='\t') //跳过空格和tab 
		{
			cmdp++;
		}
		cmd[i].args[j]=avp; 
		while(*cmdp!='\0'&&*cmdp!=' '&&
		*cmdp!='\t'&&*cmdp!='>'&&
		*cmdp!='<'&&*cmdp!='|'&&
		*cmdp!='&'&&*cmdp!='\n') //将命令字符读入 
		{
			*avp++=*cmdp++;
			inword=1;
		}
		*avp++='\0'; //分隔 
		switch(*cmdp) //根据符号判断是参数还是命令还是结束 
		{
			case ' ':
			case '\t': 
				inword=0; //空格或tab则一个参数结束 
				j++;
				break;
			case '<':
			case '>':
			case '|':
			case '&':
			case '\n':
				if(inword==0) cmd[i].args[j]=NULL; //如果不在参数中，就不是一个参数 
				return;
			default:
				return;
		}
	}
}

//检测字符 
int check(char* s)
{
	char* p;
	while(*cmdp==' '||*cmdp=='\t') //跳过空格和tab 
	{
		cmdp++;
	}
	p=cmdp;
	while(*s!='\0'&&*s==*p) //遍历搜索 
	{
		s++;
		p++;
	}
	if(*s=='\0') //命令结束 
	{
		cmdp=p;
		return 1;
	}
	return 0;
}

//获取重定向文件名 
void getname(char* name)
{
	while(*cmdp==' '||*cmdp=='\t') //跳过空格和tab 
	{
		cmdp++;
	}
	while(*cmdp!='\0'&&*cmdp!=' '&&*cmdp!='\t'&&*cmdp!='>'&&*cmdp!='<'&&*cmdp!='|'&&*cmdp!='&'&&*cmdp!='\n') //记录字符，遇到字符停止 
	{
		*name++=*cmdp++;
	}
	*name='\0'; //补字符串尾 
}

//进程管理 
void process_manage()
{
	int i;
	while(1) //使用线程轮询监控 
	{
		sleep(1);
		for(i=0;i<MAXJOB;i++) //每隔1s遍历工作组，查看其是否还在运行 
		{
			if(JOB[i]!=NULL)
			{
				char temp[1024];
				char result[1024];
				sprintf(temp,"ps -e | grep %d | wc -l",PID[i]);
				FILE* fp=popen(temp,"r");
				fgets(result,1024,fp);
				pclose(fp);
				int count=atoi(result);
				if(count==0) //若该进程已不存在，则释放掉 
				{
					if(JOB[i])
					{
						free(JOB[i]);
						JOB[i]=NULL;
					}
				}
			}
		}
	}
		
}

//系统初始化 
void init(void)
{
	setup(); //配置信号 
	int i;
	for(i=0;i<=2;i++) //为初始环境变量分配空间 
	{
		PATHVAR[i]=(char*)malloc(1024*sizeof(char));
		PATHVARNAME[i]=(char*)malloc(1024*sizeof(char));
	}
	
	//三个初始环境变量 
	strcpy(PATHVARNAME[0],"PATH");
	strcpy(PATHVARNAME[1],"PWD");
	strcpy(PATHVARNAME[2],"SHELL");
	setpath("/bin:/usr/bin");
	strcpy(PATHVAR[1],getcwd(NULL,0));
	strcpy(PATHVAR[2],"/root/myshell/myshell");
	
	//备份标准输入输出 
	fd0=dup(STDIN_FILENO);
	fd1=dup(STDOUT_FILENO);
	
	//初始化作业管理 
	for(i=0;i<MAXJOB;i++)
	{
		JOB[i]=NULL;
		PID[i]=0;
		STAT[i]=0;
	}
	pthread_create(&process_manage_id,NULL,(void*)process_manage,NULL);	//架起进程监控线程 
}

//系统收尾工作 
void end()
{
	int i;
	for(i=0;i<1024;i++) //free掉申请的空间 
	{
		if(PATHVARNAME[i]!=NULL)
		{
			free(PATHVAR[i]);
			free(PATHVARNAME[i]);
		}
		if(NORMALVARNAME[i]!=NULL)
		{
			free(NORMALVAR[i]);
			free(NORMALVARNAME[i]);
		}
	}
	for(i=0;i<MAXJOB;i++)
	{
		if(JOB[i]!=NULL) free(JOB[i]);
	}
}

//shell初始化，每条命令之前都要进行 
void shell_init()
{
	fflush(stdout); //清空标准输出 
	printf("3150104669ckczzj@myshell:%s$ ",PATHVAR[1]); //输出shell提示符
	//读取命令初始化 
	memset(cmd,0,sizeof(cmd));
	memset(cmdline,0,sizeof(cmdline));
	memset(avline,0,sizeof(avline));
	cmdp=cmdline;
	avp=avline;
	//重定向文件初始化 
	memset(infile,0,sizeof(infile));
	memset(outfile,0,sizeof(outfile));
	//标志初始化 
	cmd_count=0; 
	back=0;
	append=0;
}

//shell主循环 
void shell_loop() 
{
	while(1)
	{
		shell_init(); //初始化 
		if(read_command()==-1) break; //读取命令 
		parse_command(); //解析命令 
		execute_command();  //执行命令 
	}
	printf("\nexit\n");
}

//读取命令 
int read_command() 
{
	if(fgets(cmdline,MAXLENGTH,stdin)==NULL) //直接使用fgets读入到cmdline中 
	{
		return -1;
	}
	return 0;
}

//解析命令 
int parse_command()
{
	if(check("\n")) return 0;

	get_command(0);//解析第一条命令 

	if(check("<")) getname(infile);//判断是否有输入重定向 
	
	int i;
	for(i=1;i<PIPENUM;i++)//判断是否有管道 
	{
		if(check("|")) get_command(i);
		else break;
	}
	
	if(check(">"))//鍒判断是否有输出重定向 
	{
		if(check(">")) append=1; //是否覆盖输出 
		getname(outfile);
	}
	
	if(check("&")) back=1;//判断是否有后台作业 
	else back=0;

	if(check("\n"))//判断命令结束 
	{
		cmd_count=i; //记录命令个数 
		return cmd_count;
	}
	else
	{
		printf("Command syntax error!\n");
		return -1;
	}
}

//判断命令是否为内部命令 
int is_internal_cmd(int i)
{
	//逐一比对 
	if(strcmp(cmd[i].args[0],"pwd")==0||
	strcmp(cmd[i].args[0],"time")==0||
	strcmp(cmd[i].args[0],"clr")==0||
	strcmp(cmd[i].args[0],"dir")==0||
	strcmp(cmd[i].args[0],"quit")==0||
	strcmp(cmd[i].args[0],"cd")==0||
	strcmp(cmd[i].args[0],"echo")==0||
	strcmp(cmd[i].args[0],"environ")==0||
	strcmp(cmd[i].args[0],"umask")==0||
	strcmp(cmd[i].args[0],"declare")==0||
	strcmp(cmd[i].args[0],"set")==0||
	strcmp(cmd[i].args[0],"unset")==0||
	strcmp(cmd[i].args[0],"jobs")==0||
	strcmp(cmd[i].args[0],"help")==0||
	strcmp(cmd[i].args[0],"bg")==0) return 1;
	else return 0;
}

//执行内部命令 
void run_internal_cmd(int i)
{
	//逐一比对 
	if(strcmp(cmd[i].args[0],"pwd")==0) myshell_pwd();
	else if(strcmp(cmd[i].args[0],"time")==0) myshell_time();
	else if(strcmp(cmd[i].args[0],"clr")==0) myshell_clr();
	else if(strcmp(cmd[i].args[0],"dir")==0) myshell_dir(i);
	else if(strcmp(cmd[i].args[0],"quit")==0) myshell_quit();
	else if(strcmp(cmd[i].args[0],"cd")==0) myshell_cd(i);
	else if(strcmp(cmd[i].args[0],"echo")==0) myshell_echo(i);
	else if(strcmp(cmd[i].args[0],"environ")==0) myshell_environ();
	else if(strcmp(cmd[i].args[0],"umask")==0) myshell_umask(i);
	else if(strcmp(cmd[i].args[0],"declare")==0) myshell_declare();
	else if(strcmp(cmd[i].args[0],"set")==0) myshell_set();
	else if(strcmp(cmd[i].args[0],"unset")==0) myshell_unset();
	else if(strcmp(cmd[i].args[0],"jobs")==0) myshell_jobs();
	else if(strcmp(cmd[i].args[0],"help")==0) myshell_help();
	else if(strcmp(cmd[i].args[0],"bg")==0) myshell_bg();
}

//执行外部命令 
void run_external_cmd(int i)
{
	pid_t pid=fork(); //创建子进程 
	if(pid==0)
	{
		execvp(cmd[i].args[0],cmd[i].args); //子进程调用execvp执行外部命令 
	}
	else
	{
		waitpid(pid,NULL,0); //父进程等待子进程执行结束 
	}
	
}

//执行第i条命令 
void execute(int i)
{
	if(is_internal_cmd(i)) //若是内部命令 
	{	
		run_internal_cmd(i); //执行内部命令 
	}
	else run_external_cmd(i); //否则执行外部命令 
}

//执行第一条命令，相当于execute(0); 
void execute0()
{
	if(is_internal_cmd(0)) 
	{	
		run_internal_cmd(0);
	}
	else run_external_cmd(0);
}

//执行第一条命令的线程 
void thread1()
{
	dup2(fd[0][1],1); //将标准输出转向第一个管道的写端 
	close(fd[0][1]); //关闭原来的写端 
	if(strcmp(infile,"")!=0) //若有输入重定向 
	{
		fin=open(infile,O_RDONLY,S_IRUSR|S_IWUSR); //打开输入重定向文件 
		dup2(fin,0); //将标准输入转向输入重定向文件 
	}
	execute(0); //执行第一条命令 
}

//执行中间条命令的线程 
void thread2()
{
	int i=ii;
	dup2(fd[i-1][0],0); //将标准输入转向上一命令管道的读端 
	close(fd[i-1][0]);  
	dup2(fd[i][1],1); //将标准输出转向本次命令管道的写端 
	close(fd[i][1]);
	execute(i); //执行本次命令 
}

//执行最后一条命令的线程 
void thread3()
{
	int i=ii;
	dup2(fd[i-1][0],0); //将标准输入转向上一命令管道的读端 
	close(fd[i-1][0]);
	if(strcmp(outfile,"")!=0) //若有输出重定向文件 
	{
		if(append) fout=open(outfile,O_WRONLY|O_CREAT|O_APPEND,S_IRUSR|S_IWUSR); //追加模式 
		else fout=open(outfile,O_WRONLY|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR); //覆盖模式 
		dup2(fout,1); //将标准输出转向输出重定向文件 
	}
	else dup2(fd1,1); //否则向备份的标准输出中输出 
	execute(i); //执行最后一条命令 
}

//执行命令 
void execute_command() 
{
	setjmp(jumper); //设置跳转点，方便父进程退出函数 
	if(cmd_count==0) return; //若命令个数为0，直接返回 
	if(cmd_count==1&&is_internal_cmd(0)) //只有单内部命令才不创建新进程 
	{
		run_internal_cmd(0);
		return;
	}
	int flag=0; //标志exec 
	if(strcmp(cmd[0].args[0],"exec")==0) //若是exec命令 
	{
		int i;
		for(i=1;i<MAXARG;i++) //执行exec后面的命令 
		{
			if(cmd[0].args[i]!=NULL) strcpy(cmd[0].args[i-1],cmd[0].args[i]); 
			else break;
		}
		cmd[0].args[i-1]=NULL;
		flag=1; //标志exec 
	}
	int i;
	for(i=0;i<MAXJOB;i++) //分配工作 
	{
		if(JOB[i]==NULL) break;
	}
	pidindex=i;
	JOB[i]=(char*)malloc(1024*sizeof(char));
	strcpy(JOB[i],cmdline);
	STAT[i]=RUNNING;
	pid_t pid=fork();
	if(pid==0) //子进程执行命令 
	{
		signal(SIGTSTP,SIG_IGN); //由于父进程需要接收暂停信号，所以子进程应该忽略此信号 
		pthread_cancel(process_manage_id);
		PID[pidindex]=getpid();
		fd=(int **)malloc((PIPENUM)*sizeof(int *)); //管道申请 
		for(i=0;i<PIPENUM-1;i++)
		{
			fd[i]=(int *)malloc(2*sizeof(int));
		}
		for(i=0;i<PIPENUM-1;i++) //创建管道 
		{
			pipe(fd[i]);
		}

		if(cmd_count>=2) //命令个数超过1个 
		{
			//使用线程1执行第一条命令 
			pthread_t id1;
			pthread_create(&id1,NULL,(void*)thread1,NULL);
			pthread_join(id1,NULL);
			ii=1;
			//使用线程2执行中间命令 
			while(ii<cmd_count-1)
			{
				pthread_t id2;
				pthread_create(&id2,NULL,(void*)thread2,NULL);
				pthread_join(id2,NULL);
				ii++;
			}
			//使用线程3执行最后一条命令 
			pthread_t id3;
			pthread_create(&id3,NULL,(void*)thread3,NULL);
			pthread_join(id3,NULL);
		}	
		else if(cmd_count==1) //只有一条命令 
		{
			if(strcmp(infile,"")!=0) //若有输入重定向文件 
			{
				fin=open(infile,O_RDONLY,S_IRUSR|S_IWUSR); //打开输入重定向文件 
				dup2(fin,0); //将标准输入转向输入重定向文件 
			}
			if(strcmp(outfile,"")!=0) //若有输出重定向文件 
			{
				if(append) fout=open(outfile,O_WRONLY|O_CREAT|O_APPEND,S_IRUSR|S_IWUSR);
				else fout=open(outfile,O_WRONLY|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR);
				dup2(fout,1); //将标准输出转向输入重定向文件 
			}
			pthread_t id;
			pthread_create(&id,NULL,(void*)execute0,NULL);
			pthread_join(id,NULL);
		}
		dup2(fd0,STDIN_FILENO); //恢复标准输入 
		dup2(fd1,STDOUT_FILENO);  //恢复标准输出 
		exit(0); //退出子进程 
	}
	else //父进程监控子进程 
	{
		signal(SIGTSTP,sigtstp_handler); //接收暂停信号 
		signal(SIGCHLD,SIG_IGN);
		PID[pidindex]=pid;
		if(back==0) //若不是后台运行，则等待子进程结束 
		{
			waitpid(pid,NULL,0);
		}
	}
	if(flag) exit(0); //若是exec，退出myshel 
}



/*
 *myshell详细设计头文件 
 */

#ifndef MYSHELL_H
#define MYSHELL_H

void sigint_handler();
void sigtstp_handler();
void setup();
void setpath(char* s);
void get_command(int i);
int check(char* s);
void getname(char* name);
void process_manage();
void init();
void end();
void shell_init();
void shell_loop();
int read_command();
int parse_command();
int is_internal_cmd(int i);
void run_internal_cmd(int i);
void run_external_cmd(int i);
void execute(int i);
void execute0();
void thread1();
void thread2();
void thread3();
void execute_command();


#endif

/*
 *myshell内部命令声明头文件，格式为myshell_命令名(); 
 */

#ifndef BUILTIN_H
#define BUILTIN_H
#include <dirent.h>
 
void myshell_pwd();
void myshell_time();
void myshell_clr();
int list(DIR* dir);
void myshell_dir(int i);
void myshell_quit();
void myshell_cd(int i);
void myshell_echo(int i);
void myshell_environ();
void myshell_umask(int i);
void myshell_declare();
void myshell_set();
void myshell_unset();
void myshell_jobs();
void myshell_exec();
void myshell_help();
void myshell_bg();

#endif

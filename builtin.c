/*
 *myshell内部命令实现
 */

#include "builtin.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include "global.h"

//�ڲ�����pwd����ʾ��ǰĿ¼ 
void myshell_pwd()
{
	printf("%s\n",getcwd(NULL,0));  //���ڲ���Ҫ�洢��ǰĿ¼�����Բ���Ҫ��getcwd������� 
}

//�ڲ�����time����ʾ��ǰʱ�� 
void myshell_time()
{
	time_t t; //����ʱ������ 
	struct tm* p; //����ʱ��ṹ 
	time(&t); //���㵱ǰʱ�䣬�����1970 1 1 00:00:00������ 
	p=localtime(&t); //���ú���������ת��Ϊ����ʱ��ṹ 
	printf("%d-%d-%d %02d:%02d:%02d\n",p->tm_year+1900,p->tm_mon+1,p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec);
}

//�ڲ�����clr������
void myshell_clr()
{
	printf("%s", "\033[1H\033[2J");  //���� 
}

//dir�ĸ����������г�Ŀ¼���ļ� 
int list(DIR* dir)
{
	struct dirent* pdir; 
	while((pdir=readdir(dir))!=NULL) //һ��һ��ض�ȡĿ¼�µ��ļ� 
	{
		if(strncmp(pdir->d_name,".",1)==0||strncmp(pdir->d_name,"..",2)==0) continue; //���� . .. �Լ������ļ� 
		printf("%s\n",pdir->d_name);
	}
	return 1;
}

//�ڲ�����dir:�г�ָ��Ŀ¼�µ������ļ� 
void myshell_dir(int i)
{
	if(cmd[i].args[1]==NULL) cmd[i].args[1]="."; //��������ʱĬ��Ϊ��ǰĿ¼ 
	DIR* dir;
	if((dir=opendir(cmd[i].args[1]))==NULL) //��Ŀ¼ 
	{
		printf("Failed to opendir.\n");
		return;
	}
	list(dir); //���ø������� 
}

//�ڲ�����quit���˳�myshell 
void myshell_quit()
{
	exit(0); //�˳�myshell���� 
}

//�ڲ�����cd���л�Ŀ¼ 
void myshell_cd(int i)
{
	if(cmd[i].args[1]==NULL) //����������������ʾ��ǰĿ¼ 
	{
		myshell_pwd();
		return;
	}
	int res = chdir(cmd[i].args[1]); //�л�Ŀ¼ 
	if(res != 0)
	{
		printf("%s is nod a path,please check again. \n",cmd[i].args[1]);
		return;
	}
	strcpy(PATHVAR[1],getcwd(NULL,0)); //ά�ֻ�������PWD 
}

//�ڲ�����echo�����ָ���ַ����Լ����� 
void myshell_echo(int i)
{
	if(cmd[i].args[1]==NULL) //��������ʱʲô������ 
	{
		return;
	}
	else
	{
		char temp[1024];
		strcpy(temp,cmd[i].args[1]);
		if(temp[0]=='$') //��$����Ҫ������� 
		{
			int j;
			for(j=0;j<1024;j++) //���ݱ������ҳ��ñ��� 
			{
				if((NORMALVARNAME[j]!=NULL&&strcmp(NORMALVARNAME[j],temp+1)==0)||
				(PATHVARNAME[j]!=NULL&&strcmp(PATHVARNAME[j],temp+1)==0)) break;
			}
			if(i==1024) //δ�ҵ���ֱ�ӷ��� 
			{
				return;
			}
			else //����ñ���ֵ 
			{
				if(NORMALVARNAME[j]!=NULL&&strcmp(NORMALVARNAME[j],temp+1)==0)
				{
					printf("%s\n",NORMALVAR[j]);
				}
				else if(strcmp(PATHVARNAME[j],temp+1)==0)
				{
					printf("%s\n",PATHVAR[j]);
				}
			}
		}
		else //����ַ��� 
		{
			char result[1024];
			if(temp[0]=='"') strcpy(result,temp+1); //ȥ�������� 
			else strcpy(result,temp);
			int j;
			for(j=2;j<MAXARG;j++) //������ո�Ķ������ 
			{
				if(cmd[i].args[j]!=NULL)
				{
					strcpy(result+strlen(result)," ");
					strcpy(result+strlen(result),cmd[i].args[j]);
				}
			}
			if(result[strlen(result)-1]=='"') result[strlen(result)-1]='\0'; //ȥ�������� 
			printf("%s\n",result);
		}
	}
}

//�ڲ�����environ������������� 
void myshell_environ()
{
	int i;
	for(i=0;i<1024;i++) //���������������ǿռ���� 
	{
		if(PATHVARNAME[i]!=NULL)
		{
			printf("%s = %s\n",PATHVARNAME[i],PATHVAR[i]);
		}
		else continue;
	}
}


//�ڲ�����umask����ʾ�͸ı�Ĭ��Ȩ������ 
void myshell_umask(int i)
{
	if(cmd[i].args[1]==NULL) //��������ʱ��ʾ��ǰֵ 
	{
		int a=umask(0); //umask()����֮ǰ��ֵ�����������0��֮�������û��� 
		printf("%04o\n",a); //��ʽ��������˽�����λ��� 
		umask(a);
	}
	else //������ʱ���óɲ���ֵ 
	{
		int a=atoi(cmd[i].args[1]);
		int b=0;
    	int i=0;
        while(a!=0) //ת����ʮ���� 
	    {
	       	b+=(a%10)*pow(8,i);
			i++;
			a/=10;
		}
		umask(b);
	}
}


//�ڲ�����declare������һ����� 
void myshell_declare()
{
	if(cmd[0].args[1]==NULL) //��������ʱ�������� 
	{
		printf("Few arguments.\n");
		return;
	}
	else //������ 
	{
		char arg[1024];
		char var[1024];
		char value[1024];
		strcpy(arg,cmd[0].args[1]);
		int i;
		for(i=0;i<strlen(cmd[0].args[1]);i++) //��λ�Ⱥ� 
		{
			if(arg[i]=='=') break;
			var[i]=arg[i];
		}
		if(i==strlen(cmd[0].args[1])) //�����еȺ� 
		{
			printf("Wrong arguments.\n");
			return;
		}
		var[i]='\0'; //������ 
		int j;
		for(i=i+1,j=0;i<strlen(cmd[0].args[1]);i++,j++) //����ֵ 
		{
			value[j]=arg[i];
		}
		value[j]='\0';
		for(i=0;i<1024;i++) //�жϸñ����Ƿ��Ѵ��� 
		{
			if((NORMALVARNAME[i]!=NULL&&strcmp(NORMALVARNAME[i],var)==0)||
			(PATHVARNAME[i]!=NULL&&strcmp(PATHVARNAME[i],var)==0)) break;
		}
		if(i==1024) //�������ڣ��½� 
		{
			for(i=0;i<1024;i++)
			{
				if(NORMALVARNAME[i]==NULL) break;
			}
			NORMALVARNAME[i]=(char*)malloc(1024*sizeof(char));
			NORMALVAR[i]=(char*)malloc(1024*sizeof(char));
			strcpy(NORMALVARNAME[i],var);
			strcpy(NORMALVAR[i],value);
		}
		else //�����ڣ�Ϊ�����¸�ֵ 
		{
			if(NORMALVARNAME[i]!=NULL&&strcmp(NORMALVARNAME[i],var)==0)
			{
				strcpy(NORMALVAR[i],value);
			}
			else if(strcmp(PATHVARNAME[i],var)==0)
			{
				strcpy(PATHVAR[i],value);
			}
		}
	}	
}

//�ڲ�����set����ʾ����������Ϊȫ�ֱ��� 
void myshell_set()
{
	if(cmd[0].args[1]==NULL) //��������ʱ��ʾ���б��� 
	{
		myshell_environ(); //��ʾ�������� 
		int i;
		for(i=0;i<1024;i++) //������ʾһ����� 
		{
			if(NORMALVARNAME[i]!=NULL)
			{
				printf("%s = %s\n",NORMALVARNAME[i],NORMALVAR[i]);
			}
			else continue;
		}
	}
	else if(strcmp(cmd[0].args[1],"-a")==0) //������Ϊ-a  
	{
		if(cmd[0].args[2]==NULL) //�����еڶ������� 
		{
			printf("Few arguments.\n");
			return;
		}
		else
		{
			char var[1024];
			strcpy(var,cmd[0].args[2]);
			int i;
			for(i=0;i<1024;i++) //�жϸñ����Ƿ���� 
			{
				if(NORMALVARNAME[i]==NULL) continue;
				else if(strcmp(NORMALVARNAME[i],var)==0) break;
			}
			if(i==1024) //������ʱ�������� 
			{
				printf("No such normal variable.\n");
				return;
			}
			int j;
			for(j=0;j<1024;j++) //����ʱ����ת��λ�������� 
			{
				if(PATHVARNAME[j]==NULL) break;
			}
			PATHVARNAME[j]=(char*)malloc(1024*sizeof(char));
			PATHVAR[j]=(char*)malloc(1024*sizeof(char));
			strcpy(PATHVARNAME[j],NORMALVARNAME[i]);
			strcpy(PATHVAR[j],NORMALVAR[i]);
			free(NORMALVARNAME[i]); //ת��Ϊ�����������ͷ�ԭ�ȿռ� 
			free(NORMALVAR[i]);
			NORMALVARNAME[i]=NULL;
			NORMALVAR[i]=NULL;
		}
	}
}

//�ڲ�����dunset��ɾ��ĳ���� 
void myshell_unset()
{
	if(cmd[0].args[1]==NULL) //û�в���ʱ�������� 
	{
		printf("Few arguments.\n");
		return;
	}
	else
	{
		char var[1024];
		strcpy(var,cmd[0].args[1]);
		int i;
		for(i=0;i<1024;i++) //�жϸñ����Ƿ���� 
		{
			if((NORMALVARNAME[i]!=NULL&&strcmp(NORMALVARNAME[i],var)==0)||
			(PATHVARNAME[i]!=NULL&&strcmp(PATHVARNAME[i],var)==0)) break;
		}
		if(i==1024) //�������ڣ��������� 
		{
			printf("No such variable.\n");
			return;
		}
		else //�����ڣ��ͷ� 
		{
			if(NORMALVARNAME[i]!=NULL&&strcmp(NORMALVARNAME[i],var)==0) //��һ����� 
			{
				free(NORMALVARNAME[i]);
				free(NORMALVAR[i]);
				NORMALVARNAME[i]=NULL;
				NORMALVAR[i]=NULL;
			}
			else if(strcmp(PATHVARNAME[i],var)==0) //�ǻ������� 
			{
				free(PATHVARNAME[i]);
				free(PATHVAR[i]);
				PATHVARNAME[i]=NULL;
				PATHVAR[i]=NULL;
			}
		}
	}
}

//�ڲ�����jobs����ʾ��ǰ���� 
void myshell_jobs()
{
	int i;
	for(i=0;i<MAXJOB;i++)
	{
		if(JOB[i]!=NULL)
		{
			if(STAT[i]==RUNNING) //�������еĹ��� 
			{
				printf("Running\t\t\t%s",JOB[i]);
			}
			else if(STAT[i]==PAUSE) //��ͣ�Ĺ��� 
			{
				printf("Stopped\t\t\t%s",JOB[i]);
			}
		}
	}
}


//�ڲ�����bg������̨��ͣ�Ĺ�������ִ�� 
void myshell_bg()
{
	int i;
	for(i=0;i<MAXJOB;i++)
	{
		if(JOB[i]!=NULL&&STAT[i]==PAUSE) //�ҵ���ͣ�Ĺ��� 
		{
			char temp[1024];
			sprintf(temp,"kill -CONT %d",PID[i]); //���ͼ������е��ź� 
			STAT[i]=RUNNING; //�޸�״̬ 
			printf("%d has started.\n",PID[i]);
			system(temp);
		}
	}
}

//�ڲ�����help����ȡ�����ĵ� 
void myshell_help()
{
	system("cat /root/myshell/readme");
}
			

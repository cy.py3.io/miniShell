OBJS=main.o myshell.o builtin.o
CFLAGS=-Wall -g -lpthread -lm
myshell:$(OBJS)
	gcc $(OBJS) $(CFLAGS) -o myshell
%.o:%.c global.h myshell.h builtin.h
	gcc $(CFLAGS) -c $< -o $@
.PHONY=clean
clean:
	rm -f myshell *.o

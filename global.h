/*
 *定义一些使用的常量和全局变量 
 */

#ifndef GLOBAL_H
#define GLOBAL_H

#define MAXLENGTH 1024 //命令最大长度 
#define MAXARG 20 //参数最大个数 
#define PIPENUM 5 //管道最大数量 
#define MAXNAME 100 //文件名最大长度 
#define MAXJOB 100 //作业最大个数 
#define RUNNING 1 //作业状态 
#define PAUSE 2 //作业状态 

#include <pthread.h>
#include <setjmp.h>

char cmdline[MAXLENGTH+1]; //存储输入的命令 
char avline[MAXLENGTH+1]; //cmdline的副本 
char* cmdp; //指针指向cmdline 
char* avp; //指针指向avline 

typedef struct command command; 
struct command //命令结构 
{
	char* args[MAXARG+1]; //每个命令存储自己的参数 
};	

command cmd[PIPENUM]; //存储所有命令 

char infile[MAXNAME+1]; //存储重定向输入文件名 
char outfile[MAXNAME+1]; //存储重定向输出文件名 
int append; //重定向覆盖或追加标志 
int cmd_count; //记录命令个数 
int back; //后台运行标志 

char* PATHVAR[1024]; //记录环境变量名 
char* PATHVARNAME[1024]; //记录环境变量值
char* NORMALVAR[1024]; //记录一般变量名 
char* NORMALVARNAME[1024]; //记录一般变量值 

int** fd; //管道 
int ii;
int fin; //打开重定向输入文件的文件描述符 
int fout; //打开重定向输出文件的文件描述符 
int fd0; //保留标准输入的文件描述符 
int fd1; //保留标准输出的文件描述符 

char* JOB[MAXJOB]; //记录工作名 
int PID[MAXJOB]; //记录工作进程ID 
int STAT[MAXJOB]; //记录工作状态 
int pidindex; //记录当前执行的工作下标 
pthread_t process_manage_id; //工作监控线程id 
jmp_buf jumper; //跳转标志 

#endif

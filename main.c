#include "myshell.h"
#include "global.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc,char* argv[])
{
	if(argc==1) //不带参数时，直接运行shell 
	{
		init(); //初始化 
		shell_loop(); //shell循环 
		end(); //收尾工作 
		return 0;
	}
	else if(argc==2) //带参数时，运行脚本 
	{
		char filename[1024];
		strcpy(filename,argv[1]); //获取脚本文件名 
		FILE* fp;
		if((fp=fopen(filename,"r"))==NULL) //打开脚本文件 
		{
			printf("Error!\n");
			return -1;
		}
		init(); //初始化 
		while(1) //遍历脚本文件所有行 
		{
			//此处和shell_inti()差不多，只是少了命令提示符的输出 
			memset(cmd,0,sizeof(cmd));
			memset(cmdline,0,sizeof(cmdline));
			memset(avline,0,sizeof(avline));
			cmdp=cmdline;
			avp=avline;
			memset(infile,0,sizeof(infile));
			memset(outfile,0,sizeof(outfile));
			cmd_count=0;
			back=0;
			append=0;
			fd=(int **)malloc((PIPENUM)*sizeof(int *));
			int i;
			for(i=0;i<PIPENUM-1;i++)
			{
				fd[i]=(int *)malloc(2*sizeof(int));
			}
			for(i=0;i<PIPENUM-1;i++)
			{
				pipe(fd[i]);
			}
			if(fgets(cmdline,1024,fp)==NULL) break;
			parse_command(); //解析命令 
			execute_command(); //执行命令 
			for(i=0;i<PIPENUM-1;i++) //关闭所有管道 
			{ 
				close(fd[i][0]);
				close(fd[i][1]);
				free(fd[i]);
			}
			free(fd);
		}
		fclose(fp);
	}
}
